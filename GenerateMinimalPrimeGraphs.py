import sage.all

import sage.graphs.graph
import sage.graphs.graph_list

from MinimalPrimeGraph import *


# only works for v <= 10
# v=10 will probably crash for you though
def generate_minimal_prime_graphs_v(v):
    n = 0
    f = open("graph-g6-data/graph" + str(v) + "c.g6", "r")
    m = sage.graphs.graph_list.from_graph6(f.read())
    for g in m:
        if g.is_minimal_prime_graph():
            print("MPG" + str(v) + "_" + str(n) + " = Graph(" + str(g.to_dictionary()) + ")")
            n = n + 1


def generate_minimal_prime_graphs_le9():
    for v in range(5, 10):
        generate_minimal_prime_graphs_v(v)


def generate_minimal_prime_graphs_10c_single(v, n):
    f = open("graph-g6-data/graph10c/graph10c-" + str(v) + ".g6", "r")
    m = sage.graphs.graph_list.from_graph6(f.read())
    for g in m:
        if g.is_minimal_prime_graph():
            print("MPG" + "10" + "_" + str(n) + " = Graph(" + str(g.to_dictionary()) + ")")
            n = n + 1


def generate_minimal_prime_graphs_10c(l=10, r=69):
    n = 0
    for v in range(l, r):
        print("---" + str(v).zfill(2) + "---")
        generate_minimal_prime_graphs_10c_single(v, n)


def generate_minimal_prime_graphs_11_single(v, n):
    f = open("graph-g6-data/graph11/graph11-" + str(v).zfill(4) + ".g6", "r")
    m = sage.graphs.graph_list.from_graph6(f.read())
    for g in m:
        if g.is_minimal_prime_graph():
            print("MPG" + str(v).zfill(4) + "_" + "11" + "_" + str(n) + " = Graph(" + str(g.to_dictionary()) + ")")
            n = n + 1


def generate_minimal_prime_graphs_11():
    n = 0
    for v in range(0, 5095):
        print("---" + str(5094 - v).zfill(4) + "---", flush=True)
        generate_minimal_prime_graphs_11_single(5094 - v, n)


def generate_minimal_prime_graphs_12_euler():
    w = 0
    for n in range(0, 438):
        print("---" + str(438-n).zfill(3) + "---", flush=True)
        f = open("graph-g6-data/graph12-eulerian/x" + str(438-n).zfill(3), "r")
        m = sage.graphs.graph_list.from_graph6(f.read())
        for g in m:
            if g.is_minimal_prime_graph():
                print("MPG" + str(w).zfill(4) + "_" + "12e" + "_" + str(w) + " = Graph(" + str(g.to_dictionary()) + ")")
                w = w + 1


if __name__ == '__main__':
    generate_minimal_prime_graphs_12_euler()
