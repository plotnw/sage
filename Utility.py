import sage.graphs.graph

from MinimalPrimeGraphExamples import MPGS_L


def filter_unique_up_to_isomorphism(gs: list):
    ret = []
    for g in gs:
        # do i need this flag
        flag = True
        for h in ret:
            if g.is_isomorphic(h):
                flag = False
                break
        if flag:
            ret.append(g)
    return ret


def mpgs_to_defined_mpgs(n: int, gs: list):
    ret = []
    for g in gs:
        for h in MPGS_L[n + 1]:
            if g.is_isomorphic(h):
                ret.append(h)
    return ret


def define_mpg(g: sage.graphs.graph.Graph):
    n = len(g.vertices())
    if n < 12:
        for h in MPGS_L[n]:
            if g.is_isomorphic(h):
                g.name(h.name())
                return g
    g.name(str(g.to_dictionary()))
    return g
