import itertools

import sage.all
import sage.graphs.graph
import sage.graphs.graph_coloring

from MinimalPrimeGraph import *
from MinimalPrimeGraphExamples import *
from MPGS_15_GEN import *
from MPGS_16_GEN import *
import Utility

GRAPH_COUNTS = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
GENERATED_GRAPHS = [[], [], [], [], [], MPGS_5, MPGS_6, MPGS_7, MPGS_8, MPGS_9, MPGS_10, MPGS_11, [], [], [], [], MPGS_16_GEN]


def is_vertex_duplication(h: sage.graphs.graph.Graph, new_neighbors: list):
    new_neighbors_set = set(new_neighbors)
    for v in h:
        if set(h.neighbors(v, closed=true)) == new_neighbors_set:
            return True
    return False


def is_clique_generation(h: sage.graphs.graph.Graph, K: list):
    K_set = set(K)
    for v in h.vertices():
        if set(h.neighbors(v)).issuperset(K_set):
            return False

    # HAS TO BE AFTER MAXIMAL CHECK CUZ IT JUST RETURNS FOR SPEED PURPOSES
    # wait i dont need this check at all since it only checks for things
    # already verified to be a MPG
    # so this never returns false -_-
    #all_colorings = sage.graphs.graph_coloring.all_graph_colorings(h.complement(), 3, vertex_color_dict=True)
    #for c in all_colorings:
        #listc = []
        #for v in h.vertices():
            #listc.append(c[v])
        #if len((set(listc))) <= 2:
            #return 2
    return True


def classify_generation(g: sage.graphs.graph.Graph):
    print(g.name())
    print(str(g.to_dictionary()), flush=True)
    generated_graphs = []
    h = g.copy()
    h.add_vertex()
    n = len(h.vertices())
    for t in itertools.product([False, True], repeat=n - 1):
        h_prime = h.copy()
        new_neighbors = []
        K = []
        for i in range(0, n - 1):
            if t[i]:
                h_prime.add_edge(n - 1, i)
                new_neighbors.append(i)
            else:
                K.append(i)
        if h_prime.is_minimal_prime_graph():
            h_prime_already_found = False
            for g_gen in GENERATED_GRAPHS[n]:
                if h_prime.is_isomorphic(g_gen):
                    h_prime_already_found = True
                    h_prime = g_gen
                    break
            if not h_prime_already_found:
                h_prime.name("MPG_" + str(n) + "_" + str(GRAPH_COUNTS[n]))
                GENERATED_GRAPHS[n].append(h_prime)
                GRAPH_COUNTS[n] = GRAPH_COUNTS[n] + 1
            print("   -> " + h_prime.name() + " " + "vd:" + str(is_vertex_duplication(h, new_neighbors)) + " "
                  + "cg:" + str(is_clique_generation(h, K)) + " " + "n:" + str(new_neighbors))
            should_add = True
            for g_gen in generated_graphs:
                if g_gen.name() == h_prime.name():
                    should_add = False
                    break
            if should_add:
                generated_graphs.append(h_prime)
    print("   => ", end="")
    for g_gen in generated_graphs:
        print(g_gen.name(), end=", ")
    print("")


def check_maximal_conjecture(list):
    conjecture = True
    for g in list:
        for v in g.vertices():
            K = g.complement().neighbors(v)
            for u in g.neighbors(v):
                was_adjacent_to_all_K = True
                for w in K:
                    if not g.has_edge(u, w):
                        was_adjacent_to_all_K = False
                if was_adjacent_to_all_K:
                    conjecture = False
    return conjecture


def advance_vertex_count():
    last_vertex = len(GENERATED_GRAPHS) - 1
    GENERATED_GRAPHS.append([])
    GRAPH_COUNTS.append(0)
    for g in GENERATED_GRAPHS[last_vertex]:
        classify_generation(g)
    print("Conjecture on " + str(last_vertex + 1) + " vertices: " + str(
        check_maximal_conjecture(GENERATED_GRAPHS[last_vertex + 1])))

    for g in GENERATED_GRAPHS[last_vertex + 1]:
        print(g.name() + " = Graph(" + str(g.to_dictionary()) + ")")

    for g in GENERATED_GRAPHS[last_vertex + 1]:
        print(g.name() + ".name(\"" + g.name() + "\")")

    print("MPGS_" + str(last_vertex + 1) + "_GEN = [")
    for g in GENERATED_GRAPHS[last_vertex + 1]:
        print(g.name(), end=", ")
    print("]")


if __name__ == "__main__":
    advance_vertex_count()
