####
# PrimeGraph(G)
# 
# Computes a prime graph for a group G
# Output formatted as [[p_1, [q_{1,1}, q_{1,2}, ...]], [p_2, [q_{2, 1}, q_{2, 2}, ...]], ...]
# where the $p_i$ are the vertices and $q_{i, j}$ are the vertices $p_i$ are adjacent to
####

PrimeGraph := function(G)
    local orderList, g, primeList, ret, p, q, list, llist;
    orderList := [];
    ret := [];
    for g in AsList(G) do
        AddSet(orderList, Order(g));
    od;
    #Print(orderList);
    #Print("\n");
    primeList := PrimeDivisors(Order(G));
    #Print(primeList);
    #Print("\n");
    for p in primeList do
        list := [];
        llist := [];
        Add(list, p);
            for q in primeList do
                if ((not (p = q)) and (p*q in orderList)) then Add(llist, q); fi;
            od;
        Add(list, llist);
        Add(ret, list);
    od;
    return ret;
end;


####
# PrimeGraphSage(G)
# 
# Computes a prime graph for a group G
# Output formatted as Sage code for a Graph object
# Output printed to terminal
####

PrimeGraphSage := function(G)
    local primeGraph, q;
    primeGraph := PrimeGraph(G);
    Print("Graph({");
    for q in primeGraph do
        Print(q[1]);
        Print(": ");
        Print(q[2]);
        Print(", ");
    od;
    Print("})");
end;
