import itertools
import sage.graphs.graph

from MinimalPrimeGraph import *
from MinimalPrimeGraphExamples import *
import Utility


def bash_generation_methods(g: sage.graphs.graph.Graph, filter=True):
    ret = []
    h = g.copy()
    h.add_vertex()
    # h.relabel(range(0, len(h.vertices())))  # im like 90% sure this messes things up but its a bit late for that
    n = len(h.vertices())
    for t in itertools.product([False, True], repeat=n - 1):
        f = h.copy()
        for i in range(0, n - 1):
            if t[i]:
                f.add_edge(n - 1, i)
        if f.is_minimal_prime_graph():
            ret.append(f)
    if filter:
        return Utility.filter_unique_up_to_isomorphism(ret)
    else:
        return ret


def bash_new_generation_methods(g: sage.graphs.graph.Graph, filter=True):
    vertex_duplications = bash_vertex_duplication(g)
    generations = bash_generation_methods(g, filter)
    ret = []
    for h in generations:
        flag = True
        for k in vertex_duplications:
            if h.is_isomorphic(k):
                flag = False
                break
        if flag:
            ret.append(h)
    return ret


# kept in case its useful but i dont think this is correct way to go about this, though for all i know it gives same
# results
# the reason i was wary was maybe you take a vertex duplicated graph which has a new generation method, so the resulting
# graph might not be a base graph, yet still be of interest
def bad_bash_new_generation_methods(g: sage.graphs.graph.Graph):
    return filter(lambda x: is_base_graph(x), bash_generation_methods(g))


def bash_vertex_duplication(g: sage.graphs.graph.Graph):
    ret = []
    for v in g.vertices():
        ret.append(duplicate_vertex(g, v))
    n = len(g.vertices())
    return Utility.filter_unique_up_to_isomorphism(ret)
