import sage.all
from sage.graphs.graph import Graph

import MinimalPrimeGraph
from MinimalPrimeGraphExamples import *
from MPGS_12_GEN import *
from MPGS_13_GEN import *
from MPGS_14_GEN import *
from MPGS_15_GEN import *
from MPGS_16_GEN import *
from MPGS_17_GEN import *
BASE_GRAPHS = []
print("import sage.all")
print("from sage.graphs.graph import Graph")
print()

def pretty_print(g):
    print(g.name() + " = Graph(" + str(g.to_dictionary()) + ")")
    BASE_GRAPHS.append(g)

for g in MPGS_5:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_6:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_7:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_8:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_9:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_10:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_11:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_12_GEN:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_13_GEN:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_14_GEN:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_15_GEN:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_16_GEN:
    if g.is_base_graph():
        pretty_print(g)

for g in MPGS_17_GEN:
    if g.is_base_graph():
        pretty_print(g)
print()
print()
print("BASE_GRAPHS = [", end="")
for g in BASE_GRAPHS:
    print(g.name(), end=", ")
print("]")
